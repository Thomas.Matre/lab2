package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class Fridge implements IFridge{

    private final int maxCapacity = 20;
    private List<FridgeItem> inventory = new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        return inventory.size();
    }

    @Override
    public int totalSize() {
        return maxCapacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            return inventory.add(item);
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (inventory.contains(item)) {
            inventory.remove(item);
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        inventory.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = inventory.stream()
                .filter(FridgeItem::hasExpired)
                .collect(Collectors.toList());

        inventory.removeAll(expiredItems);
        return expiredItems;
    }
}
